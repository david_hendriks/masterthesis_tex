\select@language {english}
\vspace {-\cftbeforepartskip }
\contentsline {chapter}{\nonumberline \spacedlowsmallcaps {Abstract}}{ii}{chapter*.1}
\contentsline {chapter}{\nonumberline \spacedlowsmallcaps {Popular Abstract}}{iv}{chapter*.2}
\contentsline {chapter}{\numberline {1}\spacedlowsmallcaps {Introduction}}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Aim of this study}{2}{section.1.1}
\contentsline {chapter}{\numberline {2}\spacedlowsmallcaps {Theoretical background}}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}Single star evolution}{3}{section.2.1}
\contentsline {section}{\numberline {2.2}Black hole formation}{3}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Core collapse supernovae}{4}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}(Pulsational) Pair instability supernovae}{5}{subsection.2.2.2}
\contentsline {section}{\numberline {2.3}Binary stellar evolution}{8}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}SN kicks/Blaauw kicks}{9}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Influence of PPISN}{10}{subsection.2.3.2}
\contentsline {section}{\numberline {2.4}Gravitational waves}{10}{section.2.4}
\contentsline {section}{\numberline {2.5}Formation channels}{12}{section.2.5}
\contentsline {subsubsection}{Isolated binary evolution}{12}{section.2.5}
\contentsline {subsubsection}{Dynamical formation}{12}{section.2.5}
\contentsline {chapter}{\numberline {3}\spacedlowsmallcaps {Methods}}{14}{chapter.3}
\contentsline {section}{\numberline {3.1}Population synthesis and \textit {binary\_c}}{14}{section.3.1}
\contentsline {section}{\numberline {3.2}Initial distributions \& Fiducial model}{15}{section.3.2}
\contentsline {section}{\numberline {3.3}LBV algorithms}{17}{section.3.3}
\contentsline {subsubsection}{Fallback}{19}{figure.caption.5}
\contentsline {section}{\numberline {3.4}PPISN algorithms}{19}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}Belczynski}{20}{subsection.3.4.1}
\contentsline {subsection}{\numberline {3.4.2}Spera \& Mapelli}{21}{subsection.3.4.2}
\contentsline {section}{\numberline {3.5}Merger rate calculation}{22}{section.3.5}
\contentsline {chapter}{\numberline {4}\spacedlowsmallcaps {Black hole masses from single stars}}{24}{chapter.4}
\contentsline {section}{\numberline {4.1}Mass components and fallback}{24}{section.4.1}
\contentsline {section}{\numberline {4.2}PPISN in single star evolution}{25}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Belczynski et al.}{25}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Spera et al.}{26}{subsection.4.2.2}
\contentsline {chapter}{\numberline {5}\spacedlowsmallcaps {Validation of \textit {binary\_c}\ for the very massive stars regime in binary systems}}{27}{chapter.5}
\contentsline {section}{\numberline {5.1}Total mass distribution}{28}{section.5.1}
\contentsline {section}{\numberline {5.2}Delay time distribution}{29}{section.5.2}
\contentsline {section}{\numberline {5.3}Individual remnant masses}{30}{section.5.3}
\contentsline {chapter}{\numberline {6}\spacedlowsmallcaps {Impact of PPISN on the binary black hole mergers}}{33}{chapter.6}
\contentsline {section}{\numberline {6.1}Stars undergoing PPISN in binary systems}{33}{section.6.1}
\contentsline {section}{\numberline {6.2}PPISN comparing different algorithms}{34}{section.6.2}
\contentsline {section}{\numberline {6.3}Merger rate \& LIGO/VIRGO detectability}{42}{section.6.3}
\contentsline {chapter}{\numberline {7}\spacedlowsmallcaps {Discussion and conclusion}}{45}{chapter.7}
\contentsline {section}{\numberline {7.1}Discussion}{45}{section.7.1}
\contentsline {subsection}{\numberline {7.1.1}Computational caveats, correctness and comparison}{45}{subsection.7.1.1}
\contentsline {subsection}{\numberline {7.1.2}PPISN and the implications}{47}{subsection.7.1.2}
\contentsline {section}{\numberline {7.2}Summary \& Conclusion}{51}{section.7.2}
\contentsline {chapter}{\numberline {8}\spacedlowsmallcaps {Acknowledgements}}{53}{chapter.8}
\contentsline {chapter}{\numberline {9}\spacedlowsmallcaps {appendix}}{54}{chapter.9}
\contentsline {section}{\numberline {9.1}Single star evolution}{54}{section.9.1}
\contentsline {section}{\numberline {9.2}Mass distributions}{55}{section.9.2}
\contentsline {subsection}{\numberline {9.2.1}Extended tables of results}{55}{subsection.9.2.1}
\contentsline {subsection}{\numberline {9.2.2}Prescription Spera \& Mapelli}{59}{subsection.9.2.2}
\contentsline {section}{\numberline {9.3}\textit {binary\_c}\ settings}{60}{section.9.3}
\contentsline {chapter}{Bibliography}{62}{chapter*.30}
